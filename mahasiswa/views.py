from django.shortcuts import render
from . import models #import modul models

#querySet mengambil semua data mahasiswa
dataMhs = models.Mahasiswa.objects.all()

# Create your views here.
context = {'judul':'Web Mahasiswa',
           'pageName':'Halaman Tampil Mahasiswa',
           'kontributor':'Teddy Harfa A S',
           'mahasiswas':dataMhs}

def index(request):
    return render(request,'mahasiswa/index.html',context)