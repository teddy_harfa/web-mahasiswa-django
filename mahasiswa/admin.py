from django.contrib import admin
from mahasiswa.models import *

# Register your models here.
class MahasiswaAdmin(admin.ModelAdmin):
    list_display = ['nim','nama','alamat','jk','ps','no_tlp','email']
    list_filter = ('nim','nama','ps','jk')
    search_fields = ['nim','nama','ps','jk']
    list_per_page = 100
    
admin.site.register(Mahasiswa, MahasiswaAdmin)